// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Snake.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Components/LightComponent.h"


// Sets default values
ASnakeElementBase::ASnakeElementBase()

{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	
	
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	//Turning the lamp towards the direction of snake
	GetAttachedActors(ParentedActors);
	if (ParentedActors.Num() >0)
	{
		SnakeFlashLight = ParentedActors[0];
		if (SnakeOwner != nullptr)
		{
		YawValue = SnakeOwner->YawValue;
		
		FRotator NewRotation = FRotator(0.f, YawValue, 0.f);

		FQuat QuatRotation = FQuat(NewRotation);
		
		SnakeFlashLight->SetActorRelativeRotation(QuatRotation, false, 0, ETeleportType::None);
		}
	
	}
	

}




void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIshead)
{
	auto Snake = Cast<ASnake>(Interactor);
	if (IsValid(Snake))
	{
		
		Snake->Destroy();

	}




}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
											AActor* OtherActor,
											UPrimitiveComponent* OtherComp,
											int32 OtherBodyIndex,
											bool bFromSweep,
											const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}
//Add intens when snake eat bonus
void ASnakeElementBase::AddFlashlightIntensity(float Intensity)
{
	auto CurrIntensity = SnakeFlashLight->FindComponentByClass<ULightComponent>()->Intensity;
	auto NewIntensity = CurrIntensity + Intensity;
	if (CurrIntensity < 12000.f)
	{
		SnakeFlashLight->FindComponentByClass<ULightComponent>()->SetIntensity(NewIntensity);
	}
	
}
















