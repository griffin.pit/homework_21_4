// Fill out your copyright notice in the Description page of Project Settings.


#include "LightBonus.h"

#include "ActorSpawner.h"

// Sets default values

ALightBonus::ALightBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BonusMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponentSphere"));
	BonusMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BonusMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	
}

// Called when the game starts or when spawned
void ALightBonus::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(BonusTimerHandle, this, &ALightBonus::SetBonusVisible, 15, 1, 15);
}

// Called every frame
void ALightBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
	
	
	BonusMeshComponent->GetOverlappingComponents(ComponentsArray);           
;
	if (ComponentsArray.Num()>0)
	{
		this->SetActorHiddenInGame(1);
		FVector NewLocation(FMath::FRandRange(-1290, 1290), FMath::FRandRange(-2680, 3290), -40.000000);
		this->SetActorLocation(NewLocation);
	}



	
	

}

 void ALightBonus::OnVolumeBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{

}


 void ALightBonus::SetBonusVisible()
 {
	 if (this->IsHidden())
	 {
		 
		 this->SetActorHiddenInGame(0);
	 }
	 else
	 {
		 this->SetActorHiddenInGame(1);
		 FVector NewLocation(FMath::FRandRange(-1290, 1290), FMath::FRandRange(-2680, 3290), -40.000000);
		 this->SetActorLocation(NewLocation);
	 }

 }

 //Checking the intersection of the bonus with the snake's head
void ALightBonus::Interact(AActor* Interactor, bool bIshead)
{
	if (bIshead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		auto LightBonus = Interactor;

		if (IsValid(Snake))
		{
			auto a = Snake->SnakeElements.Num();
			
			if ((a==4) || (a==6) ||(a==8) || (a == 10))
			{
				
				Snake->SnakeElements[0]->AddFlashlightIntensity(2000.f);
			}
		}
	}
}

