// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorSpawner.h"
#include "Math/RandomStream.h"

// Sets default values

AActorSpawner::AActorSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
   
	
}

// Called when the game starts or when spawned
void AActorSpawner::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnLightBonus();
	SpawnFoodBonus();
}

// Called every frame
void AActorSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
		
	
}
//Spawn bonus at random location
void AActorSpawner::SpawnLightBonus()
{	   
	
		FVector NewLocation3(FMath::FRandRange(-1290, 1290), FMath::FRandRange(-2680, 3290), -40.000000);
		FTransform NewTransform3(NewLocation3);
		GetWorld()->SpawnActor<ALightBonus>(LightBonusBP, NewTransform3);
	
}
//Spawn bonus at random location
void AActorSpawner::SpawnFoodBonus()
{

	FVector NewLocation2(FMath::FRandRange(-1290, 1290), FMath::FRandRange(-2680, 3290), -40.000000);
	FTransform NewTransform2(NewLocation2);
	GetWorld()->SpawnActor<AFood>(FooDBonusBP, NewTransform2);

}

