// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LightBonus.h"
#include "Food.h"
#include "ActorSpawner.generated.h"



UCLASS()
class THESNAKE_API AActorSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActorSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, Category = "ActorSpawning")
		TSubclassOf<ALightBonus> LightBonusBP;
	UPROPERTY(EditDefaultsOnly, Category = "ActorSpawning")
		TSubclassOf<AFood> FooDBonusBP; 
	UPROPERTY()
		ALightBonus* LightBonus;
	UPROPERTY()
		AFood* FoodBonus;
	
	
	UFUNCTION()
		void SpawnLightBonus();

	UFUNCTION()
		void SpawnFoodBonus();
	
};
