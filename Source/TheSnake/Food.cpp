// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "DeathZone.h"
#include "ActorSpawner.h"



// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FoodBonusMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	FoodBonusMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	FoodBonusMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Checking for the intersection of the bonus with other objects of the level
	FoodBonusMeshComponent->GetOverlappingComponents(FoodComponentsArray);
	if (FoodComponentsArray.Num()>0)
		{
			this->SetActorHiddenInGame(1);
			FVector NewLocation(FMath::FRandRange(-1290, 1290), FMath::FRandRange(-2680, 3290), -40.000000);
			this->SetActorLocation(NewLocation);
			this->SetActorHiddenInGame(0);
		}
	
}

//Checking the intersection of the bonus with the snake's head
void AFood::Interact(AActor* Interactor, bool bIshead)
{
	if (bIshead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		auto Food = Interactor;

		if (IsValid(Snake))
		{
			this->SetActorHiddenInGame(1);
			Snake->AddSnakeElement();
			FVector NewLocation(FMath::FRandRange(-1290, 1290), FMath::FRandRange(-2680, 3290), -40.000000);
			this->SetActorLocation(NewLocation);
			this->SetActorHiddenInGame(0);

		}
								
	}


}

