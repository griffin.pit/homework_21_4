// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SceneMineLight.h"
#include "Snake.h"
#include "DoorOpenZone.generated.h"

UCLASS()
class THESNAKE_API ADoorOpenZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoorOpenZone();

	//UPROPERTY(EditAnyWhere)
	//	UStaticMeshComponent* ZoneMeshComponentOne;
	//UPROPERTY(EditAnyWhere)
	//	UStaticMeshComponent* ZoneMeshComponentTwo;

	//UPROPERTY(VisibleAnyWhere)
	//	ASnakeElementBase* SnakeComp;
	//UPROPERTY()
	//	TArray<UStaticMeshComponent*> ComponentsArray;

	//UPROPERTY()
	//	ASceneMineLight* SceneLight;

	//UPROPERTY()
	//	bool FirstOverlapped;
	//UPROPERTY()
	//	bool SecondOverlapped;

	//UFUNCTION()
	//	void HandleBeginOverlapOne1(UPrimitiveComponent* OverlappedComponent1,
	//							AActor* OtherActor1,
	//							UPrimitiveComponent* OtherComp1,
	//							int32 OtherBodyIndex1,
	//							bool bFromSweep1,
	//							const FHitResult& SweepResult1);

	//UFUNCTION()
	//	void HandleBeginOverlapTwo(UPrimitiveComponent* OverlappedComponent2,
	//		AActor* OtherActor2,
	//		UPrimitiveComponent* OtherComp2,
	//		int32 OtherBodyIndex2,
	//		bool bFromSweep2,
	//		const FHitResult& SweepResult2);
	//
	//UFUNCTION()
	//	void OnOverlapEndOne(UPrimitiveComponent* OverlappedComponent11,
	//		AActor* OtherActor11,
	//		UPrimitiveComponent* OtherComp11,
	//		int32 OtherBodyIndex11);

	//UFUNCTION()
	//	void OnOverlapEndTwo(UPrimitiveComponent* OverlappedComponent21,
	//		AActor* OtherActor21,
	//		UPrimitiveComponent* OtherComp21,
	//		int32 OtherBodyIndex21);



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
