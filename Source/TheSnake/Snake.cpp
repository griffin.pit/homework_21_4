// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "ActorSpawner.h"

// Sets default values

ASnake::ASnake()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EmovementDirection::LEFT;
}

void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	InputAllow = 1;
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	InputAllow = 1;
	Move();
}

void ASnake::AddSnakeElement(int ElementsNum)
{
	bool SnakeCreated;
	FVector NewLocation;


	if (SnakeElements.Num() > 0)
	{
		SnakeCreated = 1;
	}
	else
	{
		SnakeCreated = 0;
	}


	for (int i = 0; i < ElementsNum; i++)
	{
		if (SnakeCreated)
		{
			auto SnakeTail = SnakeElements[SnakeElements.Num() - 1];
			switch (LastMoveDirection)
			{
			case EmovementDirection::UP:
				NewLocation = SnakeTail->GetActorLocation();
				NewLocation.X = NewLocation.X - ElementSize;

				break;
			case EmovementDirection::DOWN:
				NewLocation = SnakeTail->GetActorLocation();
				NewLocation.X = NewLocation.X + ElementSize;

				break;
			case EmovementDirection::LEFT:
				NewLocation = SnakeTail->GetActorLocation();
				NewLocation.Y = NewLocation.Y - ElementSize;

				break;
			case EmovementDirection::RIGHT:
				NewLocation = SnakeTail->GetActorLocation();
				NewLocation.Y = NewLocation.Y + ElementSize;

				break;
			}

			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			ElemIndex = SnakeElements.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
		else
		{
			NewLocation.Set(0, SnakeElements.Num() * ElementSize * (-1), 0);
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			ElemIndex = SnakeElements.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
	}
	
}

void ASnake::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	MovementSpeed = ElementSize;
	
		switch (LastMoveDirection)
		{
		case EmovementDirection::UP:
			MovementVector.X += MovementSpeed;
			YawValue = 0.f;
			break;
		case EmovementDirection::DOWN:
			MovementVector.X -= MovementSpeed;
			YawValue = 180.f;
			break;
		case EmovementDirection::LEFT:
			MovementVector.Y += MovementSpeed;
			YawValue = 90.f;
			break;
		case EmovementDirection::RIGHT:
			MovementVector.Y -= MovementSpeed;
			YawValue = 270.f;
			break;
		}

	
	

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	FVector PrevLocation = FVector::ZeroVector;
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		 PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}

	
}



