// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeElementBase.h"
#include "TimerManager.h"
#include "Engine/EngineTypes.h"
#include "Snake.generated.h"


class ASnakeElementBase;
class AActorSpawner;

UENUM()
enum class EmovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};





UCLASS()
class THESNAKE_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY()
		AActorSpawner* as;

	UPROPERTY()
		EmovementDirection LastMoveDirection;
	UPROPERTY()
		ASnakeElementBase* ElementToRotate;
	UPROPERTY()
		float YawValue;
	UPROPERTY()
		bool NeedEat;
	UPROPERTY()
		bool NeedLight;
	UPROPERTY()
		int32 ElemIndex;
	
	
	


	//bool MoveTickEnd;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	bool InputAllow;


};
