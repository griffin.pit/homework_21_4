// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeHeadLight.h"
//#include "LightBonus.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnake;
class USpotLightComponent;

UCLASS()
class THESNAKE_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;
	UPROPERTY()
		ASnake* SnakeOwner;
	


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetFirstElementType();
		void SetFirstElementType_Implementation();

		virtual void Interact(AActor* Interactor, bool bIshead) override;

		UFUNCTION()
			void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
				AActor* OtherActor,
				UPrimitiveComponent* OtherComp,
				int32 OtherBodyIndex,
				bool bFromSweep,
				const FHitResult& SweepResult);
		UFUNCTION()
			void ToggleCollision();
		UFUNCTION()
			void AddFlashlightIntensity(float Intensity);

		UPROPERTY()
			float YawValue;
		UPROPERTY()
			AActor* SnakeFlashLight;
		UPROPERTY()
			TArray<AActor*> ParentedActors;
		
		

};

